/**
 * Created by admin on 4/30/16.
 */
RemoteApp.controller('SlideController', ['$scope', '$injector', function($scope, $injector) {
    var $rootScope = $injector.get('$rootScope');
    var $firebaseObject = $injector.get('$firebaseObject');
    var $location = $injector.get('$location');
    $scope.slide = 'list';
    $rootScope.hideIndex = false;
    $('canvas').remove();
    $scope.firstTime = true;

    Reveal.initialize({ center: true, controls: true, mouseWheel: true, transition: 'concave', dependencies: [ { src: 'vendor/reveal-js/plugin/zoom-js/zoom.js', async: true } ] });
    var fireBase = new Firebase('https://smart-remote.firebaseio.com');
    var obj = $firebaseObject(fireBase);


        obj.$watch(function(){
            angular.forEach(obj, function(value, key) {

                console.log(key, value, $scope.firstTime);
                var indexFound = value.search(/\-/g)+1;
                value = value.slice(indexFound, value.length);
                if(value.slice(value.search('-')+1, value.length).toLowerCase() === 'home' && !$scope.firstTime) {
                    $location.path('/menu');
                }
                if (key === 'presentation' && !$scope.firstTime) {

                    switch (value.toLowerCase()) {
                        case 'moveforward': Reveal.navigateNext(); break;
                        case 'movebackward': Reveal.navigatePrev(); break;
                        case 'autoplay': Reveal.navigateNext(); break;
                        case 'stopplay': Reveal.navigateNext(); break;
                    }


                } else if (key === 'presentation') {
                    $scope.firstTime = false;
                }

            });
        });







}]);