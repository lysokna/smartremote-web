/**
 * Created by admin on 4/30/16.
 */
RemoteApp.controller('MenuController', ['$scope', '$injector', function($scope, $injector) {
    $('canvas').remove();
    var $firebaseObject = $injector.get('$firebaseObject');
    var $location = $injector.get('$location');
    $scope.firstTime = true;
    $scope.choose = {
        choose1 : false,
        choose2 : true,
        choose3 : false
    };


    var fireBase = new Firebase('https://smart-remote.firebaseio.com');
    var obj = $firebaseObject(fireBase);


    obj.$watch(function(){
        angular.forEach(obj, function(value, key) {

            console.log(key, value, $scope.firstTime);
            if (key === 'menu' && !$scope.firstTime) {
                value = value.slice(value.search('-')+1, value.length);
                switch (value.toLowerCase()) {
                    case 'presentation': $scope.choose.choose3 = true; $scope.choose.choose2 = false; $scope.choose.choose1 = false; $location.path('/slideremote');  break;
                    case 'video': $scope.choose.choose3 = false; $scope.choose.choose2 = true; $scope.choose.choose1 = false; $location.path('/videoremote'); break;
                    case 'game': $scope.choose.choose3 = false; $scope.choose.choose2 = false; $scope.choose.choose1 = true; $location.path('/gameremote'); break;
                }


            } else if (key === 'menu') {
                $scope.firstTime = false;
            }

        });
    });

}]);