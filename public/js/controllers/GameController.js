/**
 * Created by admin on 4/30/16.
 */
RemoteApp.controller('GameController', ['$scope', '$injector', function($scope, $injector) {
    $scope.slide = 'list';
    // Create the canvas
    var $firebaseObject = $injector.get('$firebaseObject');
    var $timeout = $injector.get('$timeout');
    var $location = $injector.get('$location');
    $scope.firstTime = true;

    var canvas = document.createElement("canvas");
    var ctx = canvas.getContext("2d");
    canvas.width = 512;
    canvas.height = 480;
    document.body.appendChild(canvas);
// Background image
    var bgReady = false;
    var bgImage = new Image();
    bgImage.onload = function () {
        bgReady = true;
    };
    bgImage.src = "../../images/background.png";

// Hero image
    var heroReady = false;
    var heroImage = new Image();
    heroImage.onload = function () {
        heroReady = true;
    };
    heroImage.src = "../../images/hero.png";

// Monster image
    var monsterReady = false;
    var monsterImage = new Image();
    monsterImage.onload = function () {
        monsterReady = true;
    };
    monsterImage.src = "../../images/monster.png";

// Game objects
    var hero = {
        speed: 256 // movement in pixels per second
    };
    var monster = {};
    var monstersCaught = 0;

// Handle keyboard controls
    var keysDown = {};

    addEventListener("keydown", function (e) {
        keysDown[e.keyCode] = true;
    }, false);

    addEventListener("keyup", function (e) {
        delete keysDown[e.keyCode];
    }, false);

// Reset the game when the player catches a monster
    var reset = function () {
        hero.x = canvas.width / 2;
        hero.y = canvas.height / 2;

        // Throw the monster somewhere on the screen randomly
        monster.x = 32 + (Math.random() * (canvas.width - 64));
        monster.y = 32 + (Math.random() * (canvas.height - 64));
    };

    var sendKey = function(keyCode) {
        var event = document.createEvent( 'KeyboardEvent' );
        event.initKeyboardEvent( 'keydown', true, false, null, 0, false, 0, false, keyCode, 0 );
        dispatchEvent( event );
    };

// Update game objects
    var update = function (modifier) {
        if (38 in keysDown) { // Player holding up
            hero.y -= hero.speed * modifier;
        }
        if (40 in keysDown) { // Player holding down
            hero.y += hero.speed * modifier;
        }
        if (37 in keysDown) { // Player holding left
            hero.x -= hero.speed * modifier;
        }
        if (39 in keysDown) { // Player holding right
            hero.x += hero.speed * modifier;
        }

        // Are they touching?
        if (
            hero.x <= (monster.x + 32)
            && monster.x <= (hero.x + 32)
            && hero.y <= (monster.y + 32)
            && monster.y <= (hero.y + 32)
        ) {
            ++monstersCaught;
            reset();
        }
    };

// Draw everything
    var render = function () {
        if (bgReady) {
            ctx.drawImage(bgImage, 0, 0);
        }

        if (heroReady) {
            ctx.drawImage(heroImage, hero.x, hero.y);
        }

        if (monsterReady) {
            ctx.drawImage(monsterImage, monster.x, monster.y);
        }

        // Score
        ctx.fillStyle = "rgb(250, 250, 250)";
        ctx.font = "24px Helvetica";
        ctx.textAlign = "left";
        ctx.textBaseline = "top";
        ctx.fillText("Goblins caught: " + monstersCaught, 32, 32);
    };

// The main game loop
    var main = function () {
        var now = Date.now();
        var delta = now - then;

        update(delta / 1000);
        render();

        then = now;

        // Request to do this again ASAP
        requestAnimationFrame(main);
    };

// Cross-browser support for requestAnimationFrame
    var w = window;
    requestAnimationFrame = w.requestAnimationFrame || w.webkitRequestAnimationFrame || w.msRequestAnimationFrame || w.mozRequestAnimationFrame;

// Let's play this game!
    var then = Date.now();
    reset();
    main();

    var fireBase = new Firebase('https://smart-remote.firebaseio.com/game');
    fireBase.on('value', function(data) {
        console.log('data', data.val());
        remote = data.val().slice(data.val().search('-')+1, data.val().length);
        if(!$scope.firstTime) {
            var keyCode = 0;
            switch (remote) {
                case 'up':
                    keyCode = 38;
                    break;
                case 'down':
                    keyCode = 40;
                    break;
                case 'left':
                    keyCode = 37;
                    break;
                case 'right':
                    keyCode = 39;
                    break;

            }
            keysDown[keyCode] = true;
            $timeout(function(){
                    delete keysDown[keyCode];
                }
                ,150
            );

        }
        $scope.firstTime = false;
    });
    $scope.menuFirstTime = true;
    var menuRef = new Firebase("https://smart-remote.firebaseio.com/menu");
    menuRef.on('value', function(data) {
        var value = data.val();
        var splittedMenu = value.slice(value.search('-')+1, value.length).toLowerCase()
        if(splittedMenu === 'home' && !$scope.menuFirstTime) {
            $location.path('/menu');
        }
        $scope.menuFirstTime = false;
    });

}]);

