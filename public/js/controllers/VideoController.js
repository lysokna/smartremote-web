/**
 * Created by admin on 4/30/16.
 */
RemoteApp.controller('VideoController', ['$scope', '$injector', function($scope, $injector) {
    $('canvas').remove();
    $scope.slide = 'list';
    $scope.firstTime = true
    var $location = $injector.get('$location');
    $scope.menuFirstTime = true;
    $scope.playIndex = 0;
    $scope.list = {
        lists : ['http://html5videoformatconverter.com/data/images/happyfit2.mp4',
                'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4',
                'http://html5example.net/static/video/html5_Video_VP8.webm',
                'http://www.ioncannon.net/examples/vp8-webm/big_buck_bunny_480p.webm'
        ]
    };

    console.log('first');
    $(function() {
        $("#playlist li").on("click", function() {
            var searchIndex = $scope.list.lists.indexOf($(this).attr('movieurl'));
            $scope.playIndex = searchIndex;
            $("#videoarea").attr({
                "src": $(this).attr("movieurl"),
                "poster": "",
                "autoplay": "autoplay"
            })

        })
        $("#videoarea").attr({
            "src": $scope.list.lists[0],
            "poster": $("#playlist li").eq(0).attr("moviesposter")
        })
    });

    var menuRef = new Firebase ("https://smart-remote.firebaseio.com/menu");

     menuRef.on('value', function(data) {
        var value = data.val();
        var splittedMenu = value.slice(value.search('-')+1, value.length).toLowerCase()
        if(splittedMenu === 'home' && !$scope.menuFirstTime) {
            console.log(splittedMenu);
            $location.path('/menu');
        }
        $scope.menuFirstTime = false;
    });


    var videoRef = new Firebase ("https://smart-remote.firebaseio.com/video");
    videoRef.on('child_changed', function(data) {
        console.log('changed')
        if(!$scope.firstTime) {
            var value = data.val();
            if (typeof value === 'number') {
                $('video').get(0).volume = value;
            } else {
                var control = value.slice(value.search('-') + 1, value.length).toLowerCase();
                console.log(control);

                var vid = $('video');
                switch (control) {
                    case 'play':
                        vid.get(0).play();
                        break;
                    case 'pause':
                        vid.get(0).pause();
                        break;
                    case 'next':
                        var videoURL = $scope.list.lists[$scope.playIndex+1];
                        $("#videoarea").attr({
                            "src": videoURL,
                            "poster": "",
                            "autoplay": "autoplay"
                        });
                        $scope.playIndex++;
                        break;
                    case 'back':

                        var videoURL = $scope.list.lists[$scope.playIndex-1];
                        $("#videoarea").attr({
                            "src": videoURL,
                            "poster": "",
                            "autoplay": "autoplay"
                        });
                        $scope.playIndex--;
                        break;
                }
            }
        }
        $scope.firstTime = false;

    });
}]);