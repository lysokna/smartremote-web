'use strict';


var RemoteApp = angular.module('RemoteApp', ['ngRoute','firebase']);

RemoteApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/slideremote', {
                templateUrl: 'templates/pages/Slide.html',
                controller: 'SlideController'
            }).
            when('/gameremote', {
                templateUrl: 'templates/pages/Game.html',
                controller: 'GameController'
            }).
            when('/videoremote', {
                templateUrl: 'templates/pages/Video.html',
                controller: 'VideoController'
            }).
            when('/menu', {
                templateUrl: 'templates/pages/remotecategory.html',
                controller: 'MenuController'
            }).
            otherwise({
                redirectTo: '/menu'
            });
    }]);

RemoteApp.run(['$rootScope', function($rootScope){
    $rootScope.hideIndex = true;
}]);
